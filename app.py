import datetime, time

digits_list = {'0': '0️⃣', '1': '1️⃣', '2': '2️⃣', '3': '3️⃣', '4': '4️⃣', '5': '5️⃣', '6': '6️⃣', '7': '7️⃣', '8': '8️⃣', '9': '9️⃣', ':': ':'}

def time_to_emoji():
    now = datetime.datetime.now()
    time_str = f'{now.hour}:{now.minute}:{now.second}'
    time_list = list(time_str)
    for index, element in enumerate(time_list):
        time_list[index] = digits_list[element]
    time_result = ''.join(time_list)
    return time_result

if __name__ == '__main__':
    while 1:
        print(time_to_emoji())
        time.sleep(1)